<?php 
/**
 *登录日志
*/

namespace xhadmin\service\admin;

use xhadmin\CommonService;
use xhadmin\db\Log;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;

class LogService extends CommonService {


	/*
 	* @Description  登录日志管理列表数据
 	* @param (输入参数：)  {array}        where 查询条件
 	* @param (输入参数：)  {int}          limit 分页参数
 	* @param (输入参数：)  {String}       field 查询字段
 	* @param (输入参数：)  {String}       orderby 排序字段
 	* @return (返回参数：) {array}        分页数据集
 	*/
	public static function pageList($sql,$where=[],$limit,$orderby=''){
		try{
			empty($orderby) && $orderby = 'log_id desc ';
			$res = self::loadList($sql,$where,$limit,$orderby);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return ['list'=>$res['list'],'count'=>$res['count']];
	}


	/*
 	* @Description  数据导出
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function dumpData($where,$orderby){
		try{
			$sql = 'select a.*,b.name as group_name,c.user as username,c. name as nickname from cd_log as a inner join cd_group as b inner join cd_user as c on a.user_id = c.user_id and c.group_id= b.group_id';
			empty($orderby) && $orderby = 'log_id desc ';
			$res = self::loadList($sql,$where,$limit=50000,$orderby);
			$list = $res['list'];
 			if(!$list) throw new \Exception('没有数据');

			$objPHPExcel = new PHPExcel();
			//excel表头
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1','编号');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1','用户名');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1','昵称');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1','所属分组');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1','操作');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1','登录IP');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1','最后登录时间');

			//excel表内容
			foreach($list as $k=>$v){
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($k+2),$v['log_id']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($k+2),$v['username']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($k+2),$v['nickname']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($k+2),$v['group_name']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($k+2),$v['event']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($k+2),$v['ip']);
				$v['time'] = date('Y-m-d H:i:s',$v['time']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($k+2),$v['time']);
			}
			
			$filename = date('YmdHis');
			$objPHPExcel->setActiveSheetIndex(0);
			header("Content-type:text/csv"); 
			header("Content-Disposition:attachment;filename=".$filename.'.csv'); 
			header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
			header('Expires:0'); 
			header('Pragma:public');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
	}
	/*
 	* @Description  删除
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function delete($where){
		try{
			$res = Log::delete($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}




}

