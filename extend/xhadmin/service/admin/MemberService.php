<?php 
/**
 *会员管理
*/

namespace xhadmin\service\admin;

use xhadmin\CommonService;
use xhadmin\db\Member;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;

class MemberService extends CommonService {


	/*
 	* @Description  会员管理列表数据
 	* @param (输入参数：)  {array}        where 查询条件
 	* @param (输入参数：)  {int}          limit 分页参数
 	* @param (输入参数：)  {String}       field 查询字段
 	* @param (输入参数：)  {String}       orderby 排序字段
 	* @return (返回参数：) {array}        分页数据集
 	*/
	public static function pageList($where=[],$limit,$field='*',$orderby=''){
		try{
			$list = Member::loadList($where,$limit,$field,$orderby);
			$count = Member::countList($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return ['list'=>$list,'count'=>$count];
	}


	/*
 	* @Description  添加
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function add($data){
		try{
			//数据验证
			$rule = [
				'username'=>['require'],
				'password'=>['require'],
				'amount'=>['regex'=>'/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/'],
				'mobile'=>['require','unique:member','regex'=>'/^1[34578]\d{9}$/'],
				'email'=>['unique:member','regex'=>'/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/'],
				'province'=>['require'],'city'=>['require'],
			];
			//数据错误提示
			$msg = [
				'username.require'=>'用户名不能为空',
				'password.require'=>'密码不能为空',
				'amount.regex'=>'积分格式错误',
				'mobile.require'=>'手机号不能为空',
				'mobile.unique'=>'手机号已经存在',
				'mobile.regex'=>'请输入11位手机号',
				'email.unique'=>'邮箱已经存在',
				'email.regex'=>'邮箱格式错误',
				'province.require'=>'所属省不能为空',
				'city.require'=>'所属市不能为空',
			];
			self::validate($rule,$data,$msg);

			$data['password'] = md5($data['password'].config('my.password_secrect'));
			$data['create_time'] = time();
			$res = Member::createData($data);

		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function update($data){
		try{
			//数据验证
			$rule = [
				'username'=>['require'],
				'amount'=>['regex'=>'/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/'],
				'mobile'=>['require','unique:member','regex'=>'/^1[34578]\d{9}$/'],
				'email'=>['unique:member','regex'=>'/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/'],
				'province'=>['require'],'city'=>['require'],
			];
			$msg = [
				'username.require'=>'用户名不能为空',
				'amount.regex'=>'积分格式错误',
				'mobile.require'=>'手机号不能为空',
				'mobile.unique'=>'手机号已经存在',
				'mobile.regex'=>'请输入11位手机号',
				'email.unique'=>'邮箱已经存在',
				'email.regex'=>'邮箱格式错误',
				'province.require'=>'所属省不能为空',
				'city.require'=>'所属市不能为空',
			];
			self::validate($rule,$data,$msg);

			$data['create_time'] = strtotime($data['create_time']);
			$res = Member::edit($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  删除
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function delete($where){
		try{
			$res = Member::delete($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  导出
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function dumpData($where,$orderby){
		try{
			$list = Member::loadList($where,$limit=50000,$field="*",$orderby);
 			if(!$list) throw new \Exception('没有数据');

			$objPHPExcel = new PHPExcel();
			//excel表头
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1','编号');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1','用户名');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1','性别');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1','头像');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1','积分');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1','状态');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1','手机号');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1','邮箱');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1','地区');
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J1','创建时间');

			//excel表内容
			foreach($list as $k=>$v){
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.($k+2),$v['member_id']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.($k+2),$v['username']);
				$v['sex'] = getFieldVal($v['sex'],'男|1|primary,女|2|warning');
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.($k+2),$v['sex']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.($k+2),$v['headimgurl']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.($k+2),$v['amount']);
				$v['status'] = getFieldVal($v['status'],'正常|1,禁用|0');
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.($k+2),$v['status']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.($k+2),$v['mobile']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.($k+2),$v['email']);
				$v['province|city|district'] = $v['province'].'-'.$v['city'].'-'.$v['district'];
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.($k+2),$v['province|city|district']);
				$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.($k+2),$v['create_time']);
			}
			
			$filename = date('YmdHis');
			$objPHPExcel->setActiveSheetIndex(0);
			header("Content-type:text/csv"); 
			header("Content-Disposition:attachment;filename=".$filename.'.csv'); 
			header('Cache-Control:must-revalidate,post-check=0,pre-check=0');
			header('Expires:0'); 
			header('Pragma:public');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
	}


}

