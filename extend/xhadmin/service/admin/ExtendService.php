<?php 
/**
 *模型管理
*/

namespace xhadmin\service\admin;

use xhadmin\CommonService;
use xhadmin\db\Extend;
use app\admin\service\FieldSetService;

class ExtendService extends CommonService {


	/*
 	* @Description  模型管理列表数据
 	* @param (输入参数：)  {array}        where 查询条件
 	* @param (输入参数：)  {int}          limit 分页参数
 	* @param (输入参数：)  {String}       field 查询字段
 	* @param (输入参数：)  {String}       orderby 排序字段
 	* @return (返回参数：) {array}        分页数据集
 	*/
	public static function pageList($where=[],$limit,$field='*',$orderby=''){
		try{
			$list = Extend::loadList($where,$limit,$field,$orderby);
			$count = Extend::countList($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return ['list'=>$list,'count'=>$count];
	}


	/*
 	* @Description  添加
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function add($data){
		try{
			//数据验证
			$rule = [
				'title'=>['require'],
				'table_name'=>['require'],
			];
			//数据错误提示
			$msg = [
				'title.require'=>'标题不能为空',
				'table_name.require'=>'数据库表名不能为空',
			];
			self::validate($rule,$data,$msg);

			$res = Extend::createData($data);
			if($res){
				FieldSetService::createTable($data);
			}

		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function update($data){
		try{
			//数据验证
			$rule = [
				'title'=>['require'],
				'table_name'=>['require'],
			];
			$msg = [
				'title.require'=>'标题不能为空',
				'table_name.require'=>'数据库表名不能为空',
			];
			self::validate($rule,$data,$msg);
			
			$info = Extend::getInfo($data['extend_id']);
			$res = Extend::edit($data);
			if($info['table_name'] <> $data['table_name']){	
				FieldSetService::updateTable($info,$data);
			}
			
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  删除
 	* @param (输入参数：)  {array}        where 删除条件
 	* @return (返回参数：) {bool}        
 	*/
	public static function delete($where){
		try{
			$res = Extend::delete($where);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}




}

