<?php 
/**
 *会员管理
*/

namespace xhadmin\service\api;

use xhadmin\CommonService;
use xhadmin\db\Member;

class MemberService extends CommonService {


	/*
 	* @Description  账号密码登录
 	* @param (输入参数：)  {array}        username 登录账号
 	* @return (返回参数：) {bool}        
 	*/
	public static function login($data){
		try{
			$where['username'] = $data['username'];
			$where['password'] = md5($data['password'].config('my.password_secrect'));

			$res = Member::getWhereInfo($where,$field='member_id,username,mobile,create_time');
			if(!$res){
				throw new \Exception("请检查用户名或者密码");
			}
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  手机号登录
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function mobileLogin($data){
		try{
			$where['mobile'] = $data['mobile'];
			$res = Member::getWhereInfo($where,$field='member_id,username,mobile,email,create_time');
			if(!$res){
				throw new \Exception("请检查手机号");
			}
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  会员注册
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function add($data){
		try{
			//数据验证
			$rule = [
				'username'=>['require'],
				'password'=>['require'],
				'mobile'=>['require','unique:member','regex'=>'/^1[34578]\d{9}$/'],
				'email'=>['require','regex'=>'/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/'],
			];
			//错误提示消息
			$msg = [
				'username.require'=>'用户名不能为空',
				'password.require'=>'密码不能为空',
				'mobile.require'=>'手机号不能为空',
				'mobile.unique'=>'手机号已经存在',
				'mobile.regex'=>'手机号格式错误',
				'email.require'=>'邮箱不能为空',
				'email.regex'=>'邮箱格式错误',
			];
			self::validate($rule,$data,$msg);	//开始验证

			$param['username'] = $data['username'];
			$param['sex'] = $data['sex'];
			$param['headimgurl'] = $data['headimgurl'];
			$param['password'] = md5($data['password'].config('my.password_secrect'));
			$param['mobile'] = $data['mobile'];
			$param['email'] = $data['email'];

			$res = Member::createData($param);

		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function update($data){
		try{
			//数据验证
			$rule = [
				'username'=>['require'],
				'email'=>['require','regex'=>'/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/'],
			];
			//错误提示消息
			$msg = [
				'username.require'=>'用户名不能为空',
				'email.require'=>'邮箱不能为空',
				'email.regex'=>'邮箱格式错误',
			];
			self::validate($rule,$data,$msg);	//错误提示

			$param['member_id'] = $data['member_id'];
			$param['username'] = $data['username'];
			$param['email'] = $data['email'];

			$res = Member::edit($param);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改密码
 	* @param (输入参数：)  {array}        data 原始数据
 	* @return (返回参数：) {bool}        
 	*/
	public static function upPassword($data){
		try{
			if(is_null($data['password'])) throw new \Exception("密码不能为空");
			if($data['password'] <> $data['repassword']) throw new \Exception("两次密码输入不一致");
			$where['member_id'] = $data['member_id'];
			$res = Member::editWhere($where,['password'=>md5($data['password'].config('my.password_secrect'))]);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		return $res;
	}




}

