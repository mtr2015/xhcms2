<?php

return [
	'JwtAuth'	=>	app\api\http\middleware\JwtAuth::class,
    'SmsAuth'	=>	app\api\http\middleware\SmsAuth::class,
];
