<?php 
/**
 *会员管理
*/

namespace app\api\controller;

use xhadmin\service\api\MemberService;
use xhadmin\db\Member as MemberDb;
use \think\facade\Cache;
use \think\facade\Log;

class Member extends Common {


	/**
	* @api {post} /Member/login 01、账号密码登录
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  账号密码登录
	
	* @apiParam (输入参数：) {string}     		username 登录用户名
	* @apiParam (输入参数：) {string}     		password 登录密码

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function login(){
		$data = $this->_data;
		if(empty($data['username']) || empty($data['password'])) return json(['status'=>$this->errorCode,'msg'=>'账号或者密码不能为空']);
		try{
			$res = MemberService::login($data);
		}catch(\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$jwt = Jwt::getInstance();
			$jwt->setIss(config('my.jwt_iss'))->setAud(config('my.jwt_aud'))->setSecrect(config('my.jwt_secrect'))->setExpTime(config('my.jwt_expire_time'));
			$token = $jwt->setUid($res['member_id'])->encode()->getToken();

			$ret = ['status'=>$this->successCode,'data'=>$res,'token'=>$token];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'登录失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/mobileLogin 02、手机号登录
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  手机号登录
	
	* @apiParam (输入参数：) {string}     		mobile 登录手机号
	* @apiParam (输入参数：) {string}     		mobile 短信验证手机号
	* @apiParam (输入参数：) {string}     		verify_id 短信验证ID
	* @apiParam (输入参数：) {string}     		verify 短信验证码

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function mobileLogin(){
		$data = $this->_data;
		if(empty($data['mobile'])) return json(['status'=>$this->errorCode,'msg'=>'手机号不能为空']);
		try{
			$res = MemberService::mobileLogin($data);
		}catch(\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$jwt = Jwt::getInstance();
			$jwt->setIss(config('my.jwt_iss'))->setAud(config('my.jwt_aud'))->setSecrect(config('my.jwt_secrect'))->setExpTime(config('my.jwt_expire_time'));
			$token = $jwt->setUid($res['member_id'])->encode()->getToken();

			$ret = ['status'=>$this->successCode,'data'=>$res,'token'=>$token];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'登录失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/add 03、会员注册
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  会员注册
	* @apiParam (输入参数：) {string}			username 用户名 (必填) 
	* @apiParam (输入参数：) {int}				sex 性别 男|1,女|2
	* @apiParam (输入参数：) {string}			headimgurl 头像 
	* @apiParam (输入参数：) {string}			password 密码 (必填) 
	* @apiParam (输入参数：) {string}			mobile 手机号 (必填) 
	* @apiParam (输入参数：) {string}			email 邮箱 (必填) 

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码  201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","data":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":" 201","msg":"操作失败"}
	*/
	function add(){
		try {
			$res = MemberService::add($this->_data);
		} catch (\Exception $e) {
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'data'=>$res,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/update 04、修改
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  修改

	* @apiHeader {String} Authorization 用户授权token
	* @apiHeaderExample {json} Header-示例:
	* "Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjM2NzgsImF1ZGllbmNlIjoid2ViIiwib3BlbkFJZCI6MTM2NywiY3JlYXRlZCI6MTUzMzg3OTM2ODA0Nywicm9sZXMiOiJVU0VSIiwiZXhwIjoxNTM0NDg0MTY4fQ.Gl5L-NpuwhjuPXFuhPax8ak5c64skjDTCBC64N_QdKQ2VT-zZeceuzXB9TqaYJuhkwNYEhrV3pUx1zhMWG7Org"
	* @apiParam (输入参数：) {string}			username 用户名 (必填) 
	* @apiParam (输入参数：) {string}			email 邮箱 (必填) 

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码  201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":" 201","msg":"操作失败"}
	*/
	function update(){
		$data = $this->_data;
		$data['member_id'] = $data['uid'];	//token解码用户ID
		if(empty($data['member_id'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try {
			$res = MemberService::update($data);
		} catch (\Exception $e) {
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/view 05、查看会员
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  查看数据

	* @apiHeader {String} Authorization 用户授权token
	* @apiHeaderExample {json} Header-示例:
	* "Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjM2NzgsImF1ZGllbmNlIjoid2ViIiwib3BlbkFJZCI6MTM2NywiY3JlYXRlZCI6MTUzMzg3OTM2ODA0Nywicm9sZXMiOiJVU0VSIiwiZXhwIjoxNTM0NDg0MTY4fQ.Gl5L-NpuwhjuPXFuhPax8ak5c64skjDTCBC64N_QdKQ2VT-zZeceuzXB9TqaYJuhkwNYEhrV3pUx1zhMWG7Org"
	* @apiParam (输入参数：) {string}     		mobile 短信验证手机号
	* @apiParam (输入参数：) {string}     		verify_id 短信验证ID
	* @apiParam (输入参数：) {string}     		verify 短信验证码

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.data 返回数据详情
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","data":""}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"没有数据"}
	*/
	function view(){
		$data = $this->_data;
		$data['member_id'] = $data['uid'];	//token解码用户ID
		if(empty($data['member_id'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try{
			$key = md5('Member:member_id:'.$data['member_id']);
			if(Cache::get($key)){
				$res = Cache::get($key);
			}else{
				$res  = MemberDb::getInfo($data['member_id'],$field='username,sex,headimgurl,amount,status,mobile,email,province,city,district,create_time');
				Cache::set($key,$res,60);
			}
		}catch (\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'data'=>$res];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'没有数据'];
		}
		Log::info('接口输出：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/recharge 06、积分充值
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  数值加
	* @apiParam (输入参数：) {float}     		amount 充值积分

	* @apiHeader {String} Authorization 用户授权token
	* @apiHeaderExample {json} Header-示例:
	* "Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjM2NzgsImF1ZGllbmNlIjoid2ViIiwib3BlbkFJZCI6MTM2NywiY3JlYXRlZCI6MTUzMzg3OTM2ODA0Nywicm9sZXMiOiJVU0VSIiwiZXhwIjoxNTM0NDg0MTY4fQ.Gl5L-NpuwhjuPXFuhPax8ak5c64skjDTCBC64N_QdKQ2VT-zZeceuzXB9TqaYJuhkwNYEhrV3pUx1zhMWG7Org"

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.data 返回自增ID
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function recharge(){
		$data = $this->_data;
		$data['member_id'] = $data['uid'];	//token解码用户ID
		if(empty($data['member_id']) || empty($data['amount'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try{
			$where['member_id'] = $data['member_id'];
			$res = MemberDb::setInc($where,'amount',(float) $data['amount']);
		}catch(\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/backRecharge 07、积分回收
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  数值减
	* @apiParam (输入参数：) {float}     		amount 回收积分

	* @apiHeader {String} Authorization 用户授权token
	* @apiHeaderExample {json} Header-示例:
	* "Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjM2NzgsImF1ZGllbmNlIjoid2ViIiwib3BlbkFJZCI6MTM2NywiY3JlYXRlZCI6MTUzMzg3OTM2ODA0Nywicm9sZXMiOiJVU0VSIiwiZXhwIjoxNTM0NDg0MTY4fQ.Gl5L-NpuwhjuPXFuhPax8ak5c64skjDTCBC64N_QdKQ2VT-zZeceuzXB9TqaYJuhkwNYEhrV3pUx1zhMWG7Org"

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function backRecharge(){
		$data = $this->_data;
		$data['member_id'] = $data['uid'];	//token解码用户ID
		if(empty($data['member_id']) || empty($data['amount'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try{
			$where['member_id'] = $data['member_id'];
			$res = MemberDb::setDec($where,'amount',(float) $data['amount']);
		}catch(\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/upPassword 08、重置密码
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  修改密码
	* @apiParam (输入参数：) {string}     		password 新密码(必填)
	* @apiParam (输入参数：) {string}     		repassword 重复密码(必填)

	* @apiHeader {String} Authorization 用户授权token
	* @apiHeaderExample {json} Header-示例:
	* "Authorization: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOjM2NzgsImF1ZGllbmNlIjoid2ViIiwib3BlbkFJZCI6MTM2NywiY3JlYXRlZCI6MTUzMzg3OTM2ODA0Nywicm9sZXMiOiJVU0VSIiwiZXhwIjoxNTM0NDg0MTY4fQ.Gl5L-NpuwhjuPXFuhPax8ak5c64skjDTCBC64N_QdKQ2VT-zZeceuzXB9TqaYJuhkwNYEhrV3pUx1zhMWG7Org"

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function upPassword(){
		$data = $this->_data;
		$data['member_id'] = $data['uid'];	//token解码用户ID
		if(empty($data['member_id'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try {
			$res = MemberService::upPassword($data);
		} catch (\Exception $e) {
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}

	/**
	* @api {post} /Member/sendSms 09、发送短信验证码
	* @apiGroup Member
	* @apiVersion 1.0.0
	* @apiDescription  发送短信验证码
	
	* @apiParam (输入参数：) {string}     		mobile 手机号

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiParam (成功返回参数：) {string}     	array.key 返回短信验证ID
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function sendSms(){
		if(empty($this->_data['mobile'])) return json(['status'=>$this->errorCode,'msg'=>'手机号不能为空']);
		try{
			if(!preg_match('/^1[34578]\d{9}$/',$this->_data['mobile'])) return json(['status'=>$this->errorCode,'msg'=>'手机号格式错误']);
			$data['mobile']	= $this->_data['mobile'];	//发送手机号
			$data['code']	= sprintf('%06d', rand(0,999999));		//验证码
			$res = \app\api\service\sms\JuheService::sendSms($data);
		}catch(\Exception $e){
			Log::error('短信接口错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$key = md5(time().$data['mobile']);
			Cache::set($key,['mobile'=>$data['mobile'],'code'=>$data['code']],120);
			$ret = ['status'=>$this->successCode,'key'=>$key,'msg'=>'发送成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'发送失败'];
		}
		Log::info('接口返回：'.print_r($ret,true));
		return json($ret);
	}



}

