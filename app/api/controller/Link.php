<?php 
/**
 *友情链接
*/

namespace app\api\controller;

use xhadmin\service\api\LinkService;
use xhadmin\db\Link as LinkDb;
use \think\facade\Cache;
use \think\facade\Log;

class Link extends Common {


	/**
	* @api {post} /Link/index 01、首页数据列表
	* @apiGroup Link
	* @apiVersion 1.0.0
	* @apiDescription  连接名称
	* @apiParam (输入参数：) {int}     		[limit] 每页数据条数（默认20）
	* @apiParam (输入参数：) {int}     		[page] 当前页码
	* @apiParam (输入参数：) {string}		[title] 连接名称 
	* @apiParam (输入参数：) {int}			[status] 状态 正常|1|success,禁用|0|danger
	* @apiParam (输入参数：) {string}		[startTime] 创建时间开始时间
	* @apiParam (输入参数：) {string}		[endTime] 创建时间结束时间
	* @apiParam (输入参数：) {int}			[catagory_id] 分类ID 

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.data 返回数据
	* @apiParam (成功返回参数：) {string}     	array.data.list 返回数据列表
	* @apiParam (成功返回参数：) {string}     	array.data.count 返回数据总数
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","data":""}
	* @apiErrorExample {json} 02 失败示例
	* {"status":" 201","msg":"查询失败"}
	*/
	function index(){
		$limit  = $this->request->param('limit', 20, 'intval');
		$page   = $this->request->param('page', 1, 'intval');

		$where['a.title'] = $this->request->param('title', '', 'strip_tags,trim');
		$where['a.status'] = $this->request->param('status', '', 'strip_tags,trim');

		$startTime = $this->request->param('startTime', '', 'strip_tags');
		$endTime = $this->request->param('endTime', '', 'strip_tags');

		$where['a.create_time'] = \xhadmin\CommonService::getTimeWhere($startTime,$endTime);
		$where['a.catagory_id'] = $this->request->param('catagory_id', '', 'strip_tags,trim');

		$limit = ($page-1) * $limit.','.$limit;
		$field = 'a.*,b.title as class_name';
		$orderby = '';

		try{
			$key = md5(implode(',',$where).$limit.$field.$orderby);
			if(Cache::get($key)){
				$res = Cache::get($key);
			}else{
				$res['list'] = LinkDb::relateQuery($field,'catagory_id',$relate_table='link_catagory',$relate_field='catagory_id',formatWhere($where),$limit,$orderby);
				$res['count'] = LinkDb::relateQueryCount($field,'catagory_id',$relate_table='link_catagory',$relate_field='catagory_id',formatWhere($where));
				Cache::set($key,$res,60);
			}
		}catch(\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}

		Log::info('接口输出：'.print_r($res,true));
		return json(['status'=>$this->successCode,'data'=>$res]);
	}


	/**
	* @api {post} /Link/add 02、添加
	* @apiGroup Link
	* @apiVersion 1.0.0
	* @apiDescription  添加
	* @apiParam (输入参数：) {string}			title 连接名称 (必填) 
	* @apiParam (输入参数：) {string}			url 链接地址 (必填) 
	* @apiParam (输入参数：) {int}				status 状态 正常|1|success,禁用|0|danger
	* @apiParam (输入参数：) {string}			logo logo 
	* @apiParam (输入参数：) {int}				sortid 排序 
	* @apiParam (输入参数：) {int}				catagory_id 分类ID 

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码  201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","data":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":" 201","msg":"操作失败"}
	*/
	function add(){
		try {
			$res = LinkService::add($this->_data);
		} catch (\Exception $e) {
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'data'=>$res,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		return json($ret);
	}

	/**
	* @api {post} /Link/update 03、修改
	* @apiGroup Link
	* @apiVersion 1.0.0
	* @apiDescription  修改
	* @apiParam (输入参数：) {string}			title 连接名称 (必填) 
	* @apiParam (输入参数：) {string}			url 链接地址 (必填) 
	* @apiParam (输入参数：) {int}				status 状态 正常|1|success,禁用|0|danger
	* @apiParam (输入参数：) {string}			logo logo 
	* @apiParam (输入参数：) {int}				sortid 排序 
	* @apiParam (输入参数：) {string}			create_time 创建时间 
	* @apiParam (输入参数：) {int}				catagory_id 分类ID 

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码  201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":" 201","msg":"操作失败"}
	*/
	function update(){
		$data = $this->_data;
		if(empty($data['link_id'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try {
			$res = LinkService::update($data);
		} catch (\Exception $e) {
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		return json($ret);
	}

	/**
	* @api {post} /Link/delete 04、删除
	* @apiGroup Link
	* @apiVersion 1.0.0
	* @apiDescription  删除
	
	* @apiParam (输入参数：) {string}     		link_ids 主键ID

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.msg 返回成功消息
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","msg":"操作成功"}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"操作失败"}
	*/
	function delete(){
		$data = $this->_data;
		if(empty($data['link_ids'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try{
			$where['link_id'] = explode(',',$data['link_ids']);
			$res = LinkService::delete($where);
		}catch(\Exception $e){
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'msg'=>'操作成功'];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'操作失败'];
		}
		return json($ret);
	}

	/**
	* @api {post} /Link/view 05、查看数据
	* @apiGroup Link
	* @apiVersion 1.0.0
	* @apiDescription  查看数据
	
	* @apiParam (输入参数：) {string}     		link_id 主键ID

	* @apiParam (失败返回参数：) {object}     	array 返回结果集
	* @apiParam (失败返回参数：) {string}     	array.status 返回错误码 201
	* @apiParam (失败返回参数：) {string}     	array.msg 返回错误消息
	* @apiParam (成功返回参数：) {string}     	array 返回结果集
	* @apiParam (成功返回参数：) {string}     	array.status 返回错误码 200
	* @apiParam (成功返回参数：) {string}     	array.data 返回数据详情
	* @apiSuccessExample {json} 01 成功示例
	* {"status":"200","data":""}
	* @apiErrorExample {json} 02 失败示例
	* {"status":"201","msg":"没有数据"}
	*/
	function view(){
		$data = $this->_data;
		if(empty($data['link_id'])) return json(['status'=>$this->errorCode,'msg'=>'参数错误']);
		try{
			$key = md5('Link:link_id:'.$data['link_id']);
			if(Cache::get($key)){
				$res = Cache::get($key);
			}else{
				$res  = LinkDb::getInfo($data['link_id'],$field='title,url,status,logo,create_time');
				Cache::set($key,$res,60);
			}
		}catch (\Exception $e){
			Log::error('错误：'.print_r($e->getMessage(),true));
			return json(['status'=>$this->errorCode,'msg'=>$e->getMessage()]);
		}
		if($res){
			$ret = ['status'=>$this->successCode,'data'=>$res];
		}else{
			$ret = ['status'=>$this->errorCode,'msg'=>'没有数据'];
		}
		Log::info('接口输出：'.print_r($ret,true));
		return json($ret);
	}



}

