<?php

namespace app\api\service\oss;

class QiniuService
{
	
	//七牛云上传
	public function uploadQiniuyun($filePath){
		$auth = new \Qiniu\Auth(config('my.qiniuyun_access_key'), config('my.qiniuyun_secret_key'));	
		$upToken = $auth->uploadToken(config('my.qiniuyun_bucket'));
		$uploadMgr = new \Qiniu\Storage\UploadManager();		
		$key = str_replace(config('my.upload_dir'),'',config('xhadmin.domain').ltrim($filePath,'.')); //七牛云的图片保存路径 此处可以自己定义
		$key = ltrim($key,'/');
		
		try{
			$ret = $uploadMgr->putFile($upToken, $key, $filePath);
		}catch(\Exception $e){
			exit($e->getMessage());
		}
		return config('my.qiniuyun_domain').$ret[0]['key'];
	}
    
}
