<?php

namespace app\ucenter\controller;

use app\ucenter\service\AuthService;

class Login extends Admin
{

    
    /**
     * 用户登录
     * @return string
     */
    public function index()
    {
        if ($this->request->isGet()) {
            return $this->display('index');
        } else {
            
            $username = $this->request->post('username', '', 'strip_tags');
            $password = $this->request->post('password', '', 'strip_tags');
			$verify = $this->request->post('verify', '', 'strip_tags');
           
            // 用户信息验证
            try {
				if(!captcha_check($verify)){
					throw new \Exception('验证码错误');
				}
                $res = AuthService::checkLogin($username, $password);
            } catch (\Exception $e) {
                $this->error("登陆失败：{$e->getMessage()}");
            }
            $this->success('登录成功，正在进入系统...', url('ucenter/index/index'));
        }
    }
	
	/*验证码*/
	public function Verify()
	{
	    return captcha();
	}

    /**
     * 退出登录
     */
    public function out()
    {
        session('ucenter', null);
        $this->success('退出登录成功！', '@ucenter/login');
    }
	
	
}
