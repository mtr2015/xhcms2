<?php

namespace app\ucenter\controller;

class Index extends Admin
{
	
    public function index(){
		$this->view->assign('menus',$this->getSubMenu(0));
		return $this->display('index');
    }
	
	
	public function main(){
		return $this->display('main');
	}
	
	
	//生成左侧菜单栏结构列表 递归的方法
	public function getSubMenu($pid){
		$list = \app\admin\db\Menu::loadList(['status'=>1,'app_id'=>76,'pid'=>$pid]);
		if($list){
			$menus = [];
			foreach($list as $key=>$val){
				$sublist = \app\admin\db\Menu::loadList(['app_id'=>76,'status'=>1,'pid'=>$val['menu_id']]);
				if($sublist){
					$menus[$key]['sub'] = $this->getSubMenu($val['menu_id']);
				}
				$menus[$key]['title'] = $val['title'];
				$menus[$key]['icon'] = !empty($val['menu_icon']) ? $val['menu_icon'] : 'fa fa-clone';
				$menus[$key]['url'] = !empty($val['url']) ? $val['url'] : $this->getRootPathUrl().'/'.$val['controller_name'];		
			}
			return $menus;
		}	
	}
	
	public function getRootPathUrl(){
		
		$domains = config('app.domain_bind');
		if(count($domains) > 0){
			foreach($domains as $key=>$val){
				if($val == request()->app()){
					$ctxPathUrl = '';
				}else{
					$ctxPathUrl = '/'.request()->app();
				}
			}
		}else{
			$ctxPathUrl = '/'.request()->app();
		}
		
		return $ctxPathUrl;
	}
}
