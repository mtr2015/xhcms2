<?php

namespace app\ucenter\http\middleware;
use app\admin\db\Config as ConfigDb;
use think\facade\Config;

class Check
{
	
    public function handle($request, \Closure $next)
    {	
		$ucenter = session('ucenter');
        $userid = session('ucenter_sign') == data_auth_sign($ucenter) ? true : false;
        if( !$userid && ( $request->app() <> 'ucenter' || $request->controller() <> 'Login' )){
			return redirect('ucenter/Login/index');
        }
		
		//验证权限
		list($app, $controller, $action) = [$request->app(), $request->controller(), $request->action()];
		$url =  "/{$app}/{$controller}/{$action}";
		$list = ConfigDb::loadList();
		Config::set($list,'xhadmin');  //写入配置
		
        return $next($request);
    }
}