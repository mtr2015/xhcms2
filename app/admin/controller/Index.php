<?php

namespace app\admin\controller;

class Index extends Admin
{
	
    public function index(){
		$this->view->assign('menus',$this->getSubMenu(0));
		return $this->display('index');
    }
	
	public function main(){
		return $this->display('main');
	}
		
	//生成左侧菜单栏结构列表 递归的方法
	public function getSubMenu($pid){
		$list = \xhadmin\db\Node::loadList(['status'=>1,'is_menu'=>1,'pid'=>$pid],$limit=100,$field="id,title,icon,val",$orderby="sortid asc");
		if($list){
			$menus = [];
			foreach($list as $key=>$val){
			{
					$sublist = \xhadmin\db\Node::loadList(['is_menu'=>1,'status'=>1,'pid'=>$val['id']],$limit=100,$field="id,title,icon,val",$orderby="sortid asc");
					if($sublist){
						$menus[$key]['sub'] = $this->getSubMenu($val['id']);
					}
					
					if($val['id'] == 258){
						$menus[$key]['sub'] = $this->getFormList();
					}
					
					$menus[$key]['title'] = $val['title'];
					$menus[$key]['icon'] = !empty($val['icon']) ? $val['icon'] : 'fa fa-clone';
					$menus[$key]['url'] = $val['val'];		
				}
				
			}
			return $menus;
		}	
	}
	
	//获取表单菜单
	public function getFormList(){
		$where = ['status'=>1,'type'=>2];
		$menulist = db("extend")->where($where)->order('sortid desc,extend_id desc')->select();
		$menu = [];
		foreach($menulist as $key=>$val){
			$menu[$key]['title'] = $val['title'];
			$menu[$key]['icon'] = 'fa fa-clone';
			$menu[$key]['url'] = '/admin/FormData/index/extend_id/'.$val['extend_id'].'.html';	
		}
		
		return $menu;
	}
	
}
