<?php 

namespace app\admin\controller;
use app\admin\service\FieldSetService;
use app\admin\service\FieldService;
use app\admin\db\Field as FieldDb;

class Field extends Admin {


	/*数据列表*/
	function index(){
		if (!$this->request->isAjax()){
			$extend_id = $this->request->param('extend_id', '', 'intval');
			$this->view->assign('extend_id',$extend_id);
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;
			$limit = ($page-1) * $limit.','.$limit;
			
			$extend_id = $this->request->param('extend_id', '', 'intval');
			$where['extend_id'] = $extend_id;
			$field="*";
			$orderby = 'sortid asc';
			
			try{
				$res = FieldService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
				
				foreach($list as $key=>$val){
					$typeField = FieldSetService::typeField();
					$fieldInfo = $typeField[$val['type']];
					$list[$key]['type'] = $fieldInfo['name'];
				}
				
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}

	}
	
	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			FieldDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}
	
	
	/*添加字段*/
	function add(){	
		if (!$this->request->isPost()){
			$extend_id = $this->request->param('extend_id','','intval');
			empty($extend_id) && $this->error('菜单编号不能为空');
			$info['extend_id'] = $extend_id;
			$this->view->assign('info',$info);
			$this->view->assign('fieldList',FieldSetService::typeField());
			return $this->display('info');
		}else{
			$data = $this->request->post();
			try {
				$res = FieldService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
            return json(['status'=>'00','msg'=>'操作成功']);
		}
		
	}

	/*修改字段*/
	function update(){
		if (!$this->request->isPost()){
			$id = input('param.id','','intval');
			$this->view->assign('info',FieldDb::getInfo($id));
			$this->view->assign('fieldList',FieldSetService::typeField());
			return $this->display('info');
		}else{
			$data = $this->request->post();
			try {
				$res = FieldService::edit($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
            return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('id', '', 'strval');
		empty($idx) && $this->error('参数错误');
		try{
			$where['id'] = ['in',$idx];
			$res = FieldService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	
	}
	
	
	//排序上下移动操作
	function setSort(){
		$id  = $this->request->post('id', 0, 'intval');
		$type  = $this->request->post('type', 0, 'intval');
		if(empty($id) || empty($type)){
			$this->error('参数错误');
		}
		try{
			FieldService::setSort($id,$type);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	

}