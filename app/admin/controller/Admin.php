<?php

namespace app\admin\controller;
use app\BaseController;

class Admin extends BaseController
{
	
	//视图全局过滤
	public function display($tpl){
		$this->filterView();
		return $this->view->fetch($tpl);
	}
	
	public function __call($method, $args)
    {
        return json(['status'=>'01','msg'=>'方法不存在']);
    }
	
}
