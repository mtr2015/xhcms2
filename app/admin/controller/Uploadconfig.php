<?php 
/**
 *上传配置
*/

namespace app\admin\controller;

use xhadmin\service\admin\UploadconfigService;
use xhadmin\db\Uploadconfig as UploadconfigDb;

class Uploadconfig extends Admin {


	/*上传配置*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['title'] = $this->request->param('title', '', 'strip_tags,trim');
			$where['status'] = $this->request->param('status', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = UploadconfigService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			UploadconfigDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('add');
		}else{
			$data = $this->request->post();
			try {
				UploadconfigService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			if(!$id) $this->error('参数错误');
			$this->view->assign('info',checkData(UploadconfigDb::getInfo($id)));
			return $this->display('update');
		}else{
			$data = $this->request->post();
			try {
				UploadconfigService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['id'] = explode(',',$idx);
			UploadconfigService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}



}

