<?php

namespace app\admin\db;
use \think\facade\Db; 

class Field
{
	
	protected static $tableName = 'field';   //数据表名
	protected static $pk = 'id';   //主键
	
	
	/**
     * 获取列表
     * @return array 列表
     */
    public static function loadList($where=[],$limit=100,$field='*',$orderby='sortid asc,id asc'){	
		try{
			list($start,$end) = explode(',',$limit);
			$result =  Db::name(self::$tableName)->where($where)->field($field)->limit($start,$end)->order($orderby)->select();
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
    /**
     * 获取统计
     * @return int 数量
     */
    public static function countList($where){
	
		try{
			$result = Db::name(self::$tableName)->where($where)->count();
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }

    /**
     * 获取信息
     * @param int 
     * @return array 信息
     */
    public static function getInfo($pk,$field="*")
    {
        $map[self::$pk] = $pk;
        return self::getWhereInfo($map,$field,self::$pk.' desc');
    }

    /**
     * 获取信息
     * @param array $where 条件
     * @return array 信息
     */
    public static function getWhereInfo($where,$field='',$orderby='')
    {		
		try{
			empty($orderby) && $orderby = self::$pk.' desc';
			empty($field) && $field = '*';
			$result = Db::name(self::$tableName)->field($field)->where($where)->order($orderby)->find();
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
	
	/**
     * 删除信息
     * @return bool 信息
     */
    public static function delete($where)
    {	
		try{
			$result = Db::name(self::$tableName)->where($where)->delete();
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }

	/**
     * 按条件修改
     * @param array $where 条件
     * @return bool 信息
     */
    public static function editWhere($where,$data)
    {   	
		try{
			$result = Db::name(self::$tableName)->where($where)->update($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
	
	/**
     * 按主键修改
     * @return bool 信息
     */
	public static function edit($data)
    {   	
		try{
			$result = Db::name(self::$tableName)->update($data);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
	/**
     * 创建信息
     * @return array 信息
     */
    public static function createData($data)
    {	
		try{
			$result = Db::name(self::$tableName)->insertGetId($data);
		}catch(\Exception $e){
			self::setLog($e->getMessage());
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
	/**
     * 原生sql语句查询
     * @return array 信息
     */
    public static function query($sql)
    {	
		try{
			$result = Db::query($sql);
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		
		return $result;
    }
	
    
}
