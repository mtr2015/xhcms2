<?php

//接口路由文件

use think\facade\Route;

Route::rule('Member/mobileLogin', 'Member/mobileLogin')->middleware(['SmsAuth']);	//手机号登录;
Route::rule('Member/update', 'Member/update')->middleware(['JwtAuth']);	//修改;
Route::rule('Member/view', 'Member/view')->middleware(['JwtAuth','SmsAuth']);	//查看数据;
Route::rule('Member/recharge', 'Member/recharge')->middleware(['JwtAuth']);	//数值加;
Route::rule('Member/backRecharge', 'Member/backRecharge')->middleware(['JwtAuth']);	//数值减;
Route::rule('Member/upPassword', 'Member/upPassword')->middleware(['JwtAuth']);	//修改密码;
Route::rule('Base/Upload', 'Base/Upload')->middleware(['JwtAuth']);	//图片上传;
