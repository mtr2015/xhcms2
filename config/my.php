<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 自定义配置
// +----------------------------------------------------------------------
return [
	
	//后台基础配置
	'upload_dir'		=> '/uploads',
	'nocheck'			=> ['/admin/Upload/uploadimages','/admin/Login/Verify','/admin/Login/index','/admin/Index/index','/admin/Index/main','/admin/Login/out','/admin/Upload/editorUpload','/admin/Upload/uploadImages','/admin/Upload/uploadUeditor','/admin/Login/captcha'],   //不需要验证权限的url
	
	'clear_cache_dir'	=> true,		//清除缓存 true 只删除admin 后台应用  false 删除所有
	
	'password_secrect'	=> 'xhadmin',	//密码加密秘钥
	
	//api基本配置
	'api_input_log'		=> true,				//api参数输入记录日志(全局)
	'successCode'		=> '200',				//成功返回码
	'errorCode'			=> '201',				//错误返回码
	'jwtExpireCode'		=> '101',				//jwt过期
	'jwtErrorCode'		=> '102',				//jwt无效
	
	//api上传配置
	'api_upload_domain'	=> 'http://img.xhadmin.me',	//如果做本地存储 请解析一个域名到/public/upload目录  也可以不解析
	'api_upload_dir'	=> './uploads/api',		//api接口上传路径
	'api_upload_ext'	=> 'jpg,png,gif',		//api允许上传文件
	'api_upload_max'	=> 2 * 1024 * 1024,		//默认2M
	
	//聚合短信配置
	'juhe_sms_key'		=> '3420d7a7c373f4b4ac',	//key
	'verify_tpl_id'		=> '105725',	//短信验证码模板
	
	//七牛云上传配置
	'qiniuyun_status'	  => false,  // true 启用 false不启用
	'qiniuyun_access_key' => 'VrGJmgYJFLjagad1evvyolE1NtZl',    //access_key
	'qiniuyun_secret_key' => 'cezGrlfgasyFrjX_2wLCR0VFiOd2',    //secret_key
	'qiniuyun_bucket'	  => 'images',										//bucket
	'qiniuyun_domain'	  => 'http://qiniu.xhadmin.me', 			    //七牛云绑定图片访问域名 后缀加斜杠
	
	//jwt鉴权配置
	'jwt_expire_time'		=> 7200,		//token过期时间 默认2小时
	'jwt_secrect'			=> 'boTCfOGKwqTNKArT',	//签名秘钥
	'jwt_iss'				=> 'client.xhadmin',	//发送端
	'jwt_aud'				=> 'server.xhadmin',	//接收端
];
